export async function render(): Promise<void> {
    const API_KEY = '?api_key=20d7e11b247ad73856764b2c88e6bebf';
    const BASE_URL = 'https://api.themoviedb.org/3';
    const IMG_URL = 'https://image.tmdb.org/t/p/original//';
    const searchURL = BASE_URL + '/search/movie' + API_KEY;
    const FIND_URL = BASE_URL + '/find/';

    const main = document.getElementById('main');
    const submitBtn = document.getElementById('submit');
    const popular = document.getElementById('popular');
    const upcoming = document.getElementById('upcoming');
    const top_rated = document.getElementById('top_rated');
    const filmContainer = document.getElementById('film-container');
    const randomName = document.getElementById('random-movie-name');
    const randomDescription = document.getElementById(
        'random-movie-description'
    );

    popular.addEventListener('click', function () {
        const param = popular.id;
        getMovies(BASE_URL + '/movie/' + param + API_KEY);
    });

    upcoming.addEventListener('click', function () {
        const param = upcoming.id;
        getMovies(BASE_URL + '/movie/' + param + API_KEY);
    });
    top_rated.addEventListener('click', function () {
        const param = top_rated.id;
        getMovies(BASE_URL + '/movie/' + param + API_KEY);
    });

    submitBtn.addEventListener('click', (e) => {
        e.preventDefault();

        const searchTerm = search.value;
        if (searchTerm) {
            getMovies(searchURL + '&query=' + searchTerm);
        } else {
            getMovies(API_URL);
        }
    });

    function setRandomMovies(data = []) {
        if (data.length) {
            const number = Math.floor(Math.random() * data.length);
            randomName.innerHTML = data[number].original_title;
            randomDescription.innerHTML = data[number].overview;
        }
    }

    getMovies(BASE_URL + '/movie/popular' + API_KEY);
    function getMovies(url: string) {
        fetch(url)
            .then((res) => res.json())
            .then((data) => {
                if (data.results.length !== 0) {
                    showMovies(data.results);
                    setRandomMovies(data.results);
                } else {
                    main.innerHTML = `<h1 class="no-results">No Results Found</h1>`;
                }
            });
    }

    function showMovies(data: []) {
        filmContainer.innerHTML = '';
        data.forEach((movie) => {
            const { title, poster_path, overview, release_date } = movie;
            const movieEl = document.createElement('div');
            movieEl.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');
            movieEl.innerHTML = `<div class="card shadow-sm">
            <img
                src="${
                    poster_path
                        ? IMG_URL + poster_path
                        : 'http://via.placeholder.com/1080x1580'
                }" alt="${title}"
            />
            <svg
                xmlns="http://www.w3.org/2000/svg"
                stroke="red"
                fill="red"
                width="50"
                height="50"
                class="bi bi-heart-fill position-absolute p-2"
                viewBox="0 -2 18 22"
            >
                <path
                    fill-rule="evenodd"
                    d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                />
            </svg>
            <div class="card-body">
                <p class="card-text truncate">
                ${overview}
                </p>
                <div
                    class="
                        d-flex
                        justify-content-between
                        align-items-center
                    "
                >
                    <small class="text-muted">${release_date}</small>
                </div>
            </div>`;

            filmContainer.appendChild(movieEl);
        });
    }
}
