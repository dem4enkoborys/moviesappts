const API_KEY = 'api_key=20d7e11b247ad73856764b2c88e6bebf';
const BASE_URL = 'https://api.themoviedb.org/3';
const API_URL = BASE_URL + '/discover/movie?sort_by=popularity.desc&' + API_KEY;
const IMG_URL = 'https://image.tmdb.org/t/p/w500';
const searchURL = BASE_URL + '/search/movie?' + API_KEY;

const main = document.getElementById('main');
const categories = [
    {
        name: 'Popular',
        value: 'popular',
    },
    {
        name: 'Upcoming',
        value: 'upcoming',
    },
    {
        name: 'Animation',
        value: 'top_rated',
    },
];

getMovies(API_URL);

function getMovies(url: string) {
    fetch(url)
        .then((res) => res.json())
        .then((data) => {
            console.log(data.results);
            if (data.results.length !== 0) {
                showMovies(data.results);
            } else {
                main.innerHTML = `<h1 class="no-results">No Results Found</h1>`;
            }
        });
}
